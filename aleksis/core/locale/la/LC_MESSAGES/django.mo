��    6      �  I   |      �     �     �     �  	   �     �     �     �     �                    /  
   7     B     I     O     V  
   j     u     z  	   �     �     �     �     �     �  C   �          
               .     4     :     F     R     _     f     u     |  
   �     �  
   �     �     �     �     �     �     �      �     	     "     )  �  .     �     �     �     �     �     �     �     	  
   	     $	     ;	     L	     X	     e	     n	     s	     z	     �	     �	     �	     �	     �	     �	     �	     �	      
     
     &
     /
     7
     @
     T
     a
     f
     z
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
                    .     5     =     T  	   d  
   n                    ,   (              .      	   &                   1      )   +          3      "      *                                  6                 
   /      $   0         2              5          -                   %         '   #      4      !       Additional name(s) Admin Announcements Dashboard Data management Date Date and time Date of birth Description E-mail address Edit school term English First name German Group Groups Guardians / Parents Home phone Icon Impersonation Last name Login Logout Mobile phone Name Notifications Official name of the school, e.g. as given by supervisory authority People Person Persons Persons and accounts Photo Place Postal code School term School terms Search Search by name Sender Sex Short name Site description Site title Stop impersonation Street Street number System status Time Title Who should see the announcement? Write your announcement: female male Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-12-19 12:57+0000
Last-Translator: Julian <leuckerj@gmail.com>
Language-Team: Latin <https://translate.edugit.org/projects/aleksis/aleksis/la/>
Language: la
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.3.2
 addita nomines Administratio Nuntii Forum Adminstratio datarum dies Dies et hora Dies natalis Descriptio Inscriptio electronica Muta anum scolae Britannicus Primus nomen Germanus Grex Greges Parentes Numerus telephoni domi Nota Simulare aliquem Secondus nomen nomen profiteri nomen retractare Numerus telephoni mobilis Nomen Nuntii Officialis nomen scolae, e. g. Personae Persona personae Personae et computi Photographia Urbs Numerus directorius Anus scolae ani scolae Quaerere Quaerere cum breve nomine Mittens Genus Breve nomen Descriptio paginae Titulus paginae Simulandum aliquem finire Via Numerus domini Status systemae tempus Titulus Quis nuntium videatne? Scribe nuntium: femininum maskulinum 