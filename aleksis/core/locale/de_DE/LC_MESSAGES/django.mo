��    A     $    ,$      00  |   10  �   �0  ~   71  O   �1    2  d   3  3   w3  ]   �3  q   	4  p   {4    �4  �   6    �6  .   8  <   ;8  ?   x8  ^   �8  �   9  �   �9  v   �:  �   &;  +   �;  S   �;  F   ,<  I   s<  �   �<  �   A=  u   �=  �   N>  �   ?  �   �?  ^   I@  �   �@  �   vA  T   B  �   iB  �   !C  5   �C  �   �C  �   �D  �   QE  ?   F  K  TF    �G  D   �H  0  �H  �   $J  K  �J  W   L  �   lL     �L  �   rM  P   N  >   SN  r   �N  �   O  �   �O  �   iP  �   Q  2  �Q  �   �R  w   �S  J   �S  �   ET  D   DU  B   �U  �   �U  �   `V     �V     W     W  [   *W     �W     �W     �W     �W  6   �W     X     X     %X     2X     CX     UX     ]X  
   mX     xX     �X     �X     �X     �X     �X     �X     �X     �X     Y     5Y     ;Y     RY  	   bY  .   lY     �Y  %   �Y  #   �Y  I   �Y  *   5Z     `Z     mZ     �Z     �Z     �Z  <   �Z  7   �Z  -   +[     Y[     w[     �[  	   �[     �[  .   �[  H   �[     9\     M\     _\     k\     p\     �\     �\     �\     �\     �\  	   �\  	   �\     �\  !   �\     ]  !   :]     \]     y]     �]     �]     �]     �]     �]     ^     5^     E^     Y^     w^     �^     �^     �^     �^     �^     _     _     4_      C_     d_     {_     �_     �_     �_  
   �_     �_     �_  !   `     #`     4`     =`     C`  	   U`     _`     m`     y`     �`     �`     �`     �`     �`     �`  7   �`     a  "   a     9a     Ia     [a     ua     �a     �a     �a     �a     �a     �a     �a     	b     #b     /b     @b     Rb  	   _b     ib     zb     �b     �b     �b     �b     �b     �b     c     c     c     $c     2c      Rc  	   sc     }c     �c     �c     �c     �c     �c     �c     
d     d     (d     ;d     Gd     Xd     jd  !   rd     �d     �d     �d     �d     �d     �d     �d     �d     e     e     )e     8e  
   Oe     Ze     je     ve  M   �e  5   �e  /   f      ;f     \f  &   jf     �f     �f  1   �f     �f     �f     �f     g     g     2g     Hg  .   Pg  8   g     �g     �g     �g  
   �g     �g  5   h  p   7h     �h  :   �h     �h     i     i     4i     Hi     [i     si     zi     �i     �i  
   �i     �i     �i     �i     �i     �i     �i     �i  
   �i  R   �i     Qj  
   Tj     _j     dj  _   mj  u   �j     Ck     Rk     ^k     ok     }k     �k     �k     �k     �k     �k     �k  %   �k  	    l     
l     l     #l     <l     Rl     bl     yl     �l     �l     �l     �l      �l     �l     �l  	   �l     �l     m     m     +m     Dm     Gm     Om     Tm     \m     im     zm     �m     �m     �m     �m     �m     �m     �m     n     n     n     8n     Qn     mn     ~n     �n     �n  0   �n     o     o     $o     6o     Do     Xo  C   lo     �o  1   �o     �o     p     p     p      p     )p  	   Fp     Pp     Yp     gp     �p     �p     �p     �p     �p     �p     �p     �p     q     q     !q     'q     -q  l   @q  h   �q  !   r  S   8r     �r  6   �r     �r     �r     s     s     *s     9s     Gs     cs     rs     �s     �s     �s     �s     �s     �s  	   t  
   t     t     't     At  ;   St     �t     �t     �t     �t     �t     �t     �t     �t     �t     �t     u     u     4u     @u     Mu     Tu     `u     zu     �u     �u     �u     �u  *   �u     �u     �u     v     
v     v  
   v  
   &v  %   1v     Wv     dv     pv     xv     v     �v     �v     �v  
   �v     �v     �v     �v     w  /   -w     ]w  ,   iw  
   �w  
   �w     �w     �w     �w     �w     �w     �w     �w     x     x     x  	   &x     0x     Ex     [x     ox     �x  6   �x  -   �x  &   �x  $   y  "   By      ey  8   �y      �y  )   �y  G   
z  &   Rz  &   yz  $   �z  &   �z  D   �z     1{     M{      g{     �{  L   �{  �   �{     �|     �|  -   �|  5   }  !   T}     v}  3   �}  1   �}  5   �}  1   2~  7   d~  `   �~  ;   �~     9  #   U  D   y  2   �  .   �  !    �  "   B�     e�  �   z�      ��  '   �     D�  
   I�     T�     Z�     i�  )   w�  1   ��  .   Ӂ  �   �     ��     ��     ��  
   ��     ��  
   Ă     ς     ւ     �     ��     �  
   �     &�     2�     L�  V   _�     ��      Ƀ     �     ��     �  e   !�  �   ��  E   �  M   Y�  C   ��  E   �  $   1�  M   V�  *   ��      φ  �   ��     ��     ��  9   ��     �  
   �     �     ��  "    �     #�     *�     3�  �  Q�  ~   �  �   ��  �   �  S   ��    �  p   ��  5   i�  u   ��  z   �  q   ��  4  �    7�    O�  1   f�  >   ��  >   ג  n   �  �   ��  .  M�  �   |�  �   �  -   ��  M   ߖ  B   -�  H   p�  �   ��  �   ;�  �   ̘  �   l�  �   X�  �   �  g   ��  �   �  �   �  b   ��  �   ��  �   ͞  5   f�  �   ��  �   .�  �   "�  3   �  �  �  E  ��  N   �  q  @�  �   ��  X  ��  _   �  �   J�  �   �  �   e�  Y   �  :   r�  �   ��  �   9�  �   Ѭ  �   ��  �   b�  j  '�  �   ��  q   M�  H   ��  S  �  K   \�  I   ��  �   �  �   ��      P�     q�     u�  i   ��     ��     �     �     4�  6   B�     y�     �  
   ��     ��     ��     ��     ��     Ҷ  
   ߶     �     �  #   �     C�     V�     j�     }�      ��      ��     Ƿ     ͷ     ��      �  .   �     =�  *   F�  .   q�  W   ��  4   ��     -�     :�     V�     r�  	   ��  O   ��  R   ۹  >   .�     m�     ��     ��     ��     ĺ  5   ݺ  [   �     o�     ��     ��     ��     ��     ϻ     �     ��     �     %�     2�  
   D�     O�  !   a�  "   ��  $   ��  *   ˼  *   ��     !�     <�  !   \�     ~�  ,   ��     ��     �     ��  "   �     8�  $   X�     }�     ��     ��     ��     ۾     ��     �  %   )�     O�  	   g�     q�     ��     ��     ��     ��     ܿ  !   ��     �     2�     9�     G�  	   Z�     d�  
   r�     }�     ��     ��     ��     ��     ��     ��  B   ��     !�  &   7�     ^�     q�     ��     ��     ��     ��     ��     �     �     .�     B�     S�     p�      ��      ��     ��  	   ��     ��     �     �  !   $�  "   F�     i�     {�     ��     ��     ��     ��     ��  #   ��     �     �     ,�     9�     W�     w�     ��     ��     ��     ��     ��     ��     ��     �     �     +�  )   8�  	   b�     l�     ��     ��     ��  
   ��     ��     ��     ��     �     �     0�     N�     _�     t�     ��  c   ��  ;   ��  3   :�  (   n�     ��  8   ��     ��     ��  =   ��     9�     @�     _�     n�     ��     ��     ��      ��  C   ��     �     .�     >�     O�     W�  T   k�  �   ��     G�  9   `�     ��     ��     ��     ��     ��     �     "�     *�     8�     @�  
   G�     R�     _�     g�     �     ��     ��  
   ��     ��  \   ��     �  
    �     +�  
   2�  ^   =�  �   ��     0�  
   C�     N�     f�  	   r�  
   |�     ��     ��     ��     ��     ��  )   ��     	�     �     &�  '   +�     S�     r�     ��     ��     ��     ��     ��     ��  '   ��     #�     (�     1�     :�     Q�     b�     |�     ��  
   ��     ��     ��     ��     ��  $   ��     ��     �     �      �     :�     I�     U�     p�     �  &   ��     ��      ��     ��     �     �  ,   =�  F   j�     ��     ��     ��     ��     �     �  H   -�     v�  8   ��     ��     ��     ��     ��  	   ��     �     �     *�     3�     J�     f�     o�     ��  1   ��  2   ��     ��     �     �     �     $�     8�     =�     A�  �   ]�  z   ��  '   Y�  U   ��  2   ��  H   
�     S�     `�     v�     ��     ��     ��     ��     ��     ��  !   �  '   )�     Q�  "   o�     ��     ��  
   ��  
   ��     ��     ��     ��  D   �  !   Y�     {�     ��  
   ��     ��     ��     ��     ��  	   ��     ��     �  +    �  	   L�  
   V�     a�     h�     u�     ��     ��     ��     ��     ��  7   ��     $�     -�     6�     =�  
   M�     X�     a�  (   p�     ��     ��     ��     ��     ��     ��     
�     �     9�     E�  #   a�     ��     ��  1   ��  	   ��  ,   ��  
   �     '�     3�     :�     N�  
   V�     a�     i�     z�     ��     ��     ��     ��     ��     ��     ��     �     �  ;   &�  *   b�  &   ��  (   ��     ��  #   ��  <   �  "   \�  $   �  I   ��  $   ��  %   �  '   9�  3   a�  O   ��     ��     �     �  !   ?�  [   a�  �   ��     ��     ��  $   ��  Q   ��     I�      g�  5   ��  ,   ��  =   ��  9   )�  7   c�  k   ��  3   �  %   ;�  !   a�  K   ��  <   ��  7   �  2   D�  3   w�     ��  �   ��  )   R�  (   |�     ��     ��     ��  	   ��     ��  *   ��  2   ��  3   -�  �   a�     �  
   �     (�  
   ,�  	   7�     A�     J�     X�     n�  (   ��     ��     ��     ��  $   ��     ��  c   �     s�      ��     ��     ��      ��  x   ��  �   a�  i   �  X   n�  j   ��  G   2�  %   z�  |   ��  2   �  +   P�  �   |�     (�     9�  :   G�     ��  
   ��  	   ��     ��  .   ��     ��  	   ��     ��     �          �   ?    �       *         �  ,  Y   C          9              �  �   
   }            z   �       �  I  ]  F          �   B   <          �      h       �   �     �       �       �      �   �       4      E   �  /   �   �  �    �      ~   �           S  �   �   *     �      Z  �   [      >  r   �   '       .      2  f   +      �      �                �  �  �       i   �       a     �       V  �  �  �   �                 �  �  -   �       �  8  �   �         �      X      �      �   G   i  ]   :    �      �  #  �   V           ,  �   �       �      =          ;  <   �  '  (      �  �  �           7     W   �       �  )      �  �  �       |  1    �   �               Q     �      {   #   	          �   �       �   �  J      	  s       �       �       A   �  �   e      3      [     Z       �   �    �           m          �   �        n  �  e  �  K  �      �  6              �  �       
  S   �      �  �   T        _   a   k  �    �   �   �  $   �       �       �   )  �  �  &  �  P  `       �  �  �           H  q   `  �  �   �   +  6   �  ;   �    �  x  �  d  !     #  ^       "      �             �            �          n   p       �  �        D  ^          \   �  �   �  y  y             C   �  v       �   �   �     �   5   !  �  �     �       �  d   m      b   u     7  �  @  9     �      �      L      }            w   F   ?   �  /      5  �  1      �   j           %  �   �      T   �   X   �        �       �   x       \  o     �  �   �               �  �   O      R   �  �  >   �      c   �     �   8      �  (  �  �  �   q  �        A  K       -          l  6  �   �  %          f  Q   �   :     R  �    �  �   �   Y  �           ~      ;          �   �   �                c  )   w      �   �   �   �     g  �   ?  �   4  �  �     �         >  4   '      2  8   �  �          N   $        �   :         �   �  �   &                   �  .       �   O              1  P   *  	  �   �      �   M   H         L             �      M  �     �  (       �  �  �   =  3      2           �        %   0  �      9          U  �      �  +   k   0   �   �      �  �   j      �   �       �   r  �   o        �    <  �    b          J     �  �   �   �   �     �  0            �   E  _  �      �   �   3  t    �   �  �   �      �           !   �           $      �              �    p  @   U                 I   |   "        �  u      �   s      �  �  @  �  h  /  =  G  �   �       t       z                     �   
     "   v  �  �          g     W      �   �  �   A    �  -      �   D   �   �     �      �          �      �          {  �  �           �  �  �   �   �          �   �   �   �   l   5  ,       B  �   7  &       .  �    �      N   
                      Please enter the tokens generated by your token
                      generator.
                     
                      We are calling your phone right now, please enter the
                      digits you hear.
                     
                      We sent you a text message, please enter the tokens we
                      sent.
                     
                    This app is licenced under %(licence)s.
                   
                    Use this form for entering backup tokens for logging in.
                    These tokens have been generated for you to print and keep safe. Please
                    enter one of these backup tokens to login to your account.
                   
                Debug mode is disabled. Default error pages are displayed on errors.
               
                Login with %(name)s
               
                Only admin and visitors from internal IPs can access thesite.
               
                The web server throws back debug information on errors. Do not use in production!
               
              If this issue persists, please contact one of your site
              administrators
             
              The core and the official apps of AlekSIS are licenced under the EUPL, version 1.2 or later. For licence
              information from third-party apps, if installed, refer to the respective components below. The
              licences are marked like this:
             
              The password reset link was invalid, possibly because it has already been used. Please request a <a href="%(passwd_reset_url)s"
              class="blue-text text-lighten-2">new password reset</a>.
             
              This platform is powered by AlekSIS, a web-based school information system (SIS) which can be used
              to manage and/or publish organisational artifacts of educational institutions. AlekSIS is free software and
              can be used by anyone.
             
              Valid for %(from)s
             
              Valid for %(from)s – %(until)s
             
              Valid from %(from)s until %(until)s
             
              Without activated JavaScript the progress status can't be updated.
             
            An error occurred while attempting to login via your third-party account.
            Please contact one of your site administrators.
           
            If you click "Back" or "Next" the current group assignments are not saved.
            If you click "Save", you will overwrite all existing child group relations for this group with what you
            selected on this page.
           
            If you think this is an error in AlekSIS, please contact your site
            administrators:
           
            If you were redirected by a link on an external page,
            it is possible that that link was outdated.
           
            Login with %(name)s
           
            Please select some groups in order to go on with assigning.
           
            Sent by %(trans_sender)s at %(trans_created_at)s
         
            The system will check for the following problems:
           
            There is a problem getting the widget "%(title)s".
            There is no need for you to take any action.
           
            This account is currently inactive. If you think this is an
            error, please contact one of your site administrators.
           
            This page is currently unavailable. If this error persists, contact your site administrators:
           
            This part of the site requires us to verify that you are who you claim to be.
            For this purpose, we require that you verify ownership of your e-mail address.
           
            This sign up is currently closed. If you think this is an
            error, please contact one of your site administrators.
           
            Users are not allowed to edit their own passwords. If you think
            this is an error please contact one of your site administrators.
           
            We are calling your phone right now, please enter the digits you hear.
           
            We have sent an e-mail to you for verification.
            Please click on the link inside this e-mail. Please
            contact us if you do not receive it within a few minutes.
           
            We have sent you an e-mail. Please contact one of your site
            administrators if you do not receive it within a few minutes.
           
            We sent you a text message, please enter the tokens we sent.
           
            You decided to cancel logging in to our site using one of your existing accounts. If this was a mistake, please proceed to <a href="%(login_url)s">sign in</a>.
           
            Your administrator account is not linked to any person. Therefore,
            a dummy person has been linked to your account.
           
            Your password is now changed!
           
            Your site administrators will automatically be notified about this
            error. You can also contact them directly:
           
            Your user account is not linked to a person. This means you
            cannot access any school-related information. Please contact
            the managers of AlekSIS at your school.
           
          However, it might happen that you don't have access to
          your primary token device. To enable account recovery, generate backup codes
          or add a phone number.
         
          No third-party account providers available.
         
          On this page you can arrange the default dashboard which is shown when a user doesn't arrange his own
          dashboard. You can drag any items from "Available widgets" to "Default Dashboard" or change the order
          by moving the widgets. After you have finished, please don't forget to click on "Save".
         
          On this page you can arrange your personal dashboard. You can drag any items from "Available widgets" to "Your
          Dashboard" or change the order by moving the widgets. After you have finished, please don't forget to click on
          "Save".
         
          Print these tokens and keep them somewhere safe.
         
          We've encountered an issue with the selected authentication method. Please
          go back and verify that you entered your information correctly, try
          again, or use a different authentication method instead. If the issue
          persists, contact the site administrator.
         
          You can use this to assign child groups to groups. Please use the filters below to select groups you want to
          change and click "Next".
         
        Backup tokens can be used when your primary and backup
        phone numbers aren't available. The backup tokens below can be used
        for login verification. If you've used up all your backup tokens, you
        can generate a new set of backup tokens. Only the backup tokens shown
        below will be valid.
       
        Congratulations, you've successfully enabled two-factor authentication.
       
        However we strongly discourage you to do so, you can
        also disable two-factor authentication for your account.
       
        Please enter the phone number you wish to be called on.
        This number will be validated in the next step.
       
        Please enter the phone number you wish to receive the
        text messages on. This number will be validated in the next step.
       
        Please select which authentication method you would like to use:
       
        Sent by %(trans_sender)s at %(trans_created_at)s
     
        This simple view can be used to ensure the correct function of the built-in PDF generation system.
       
        To identify and verify your YubiKey, please insert a
        token in the field below. Your YubiKey will be linked to your
        account.
       
        To start using a token generator, please use your
        favourite two factor authentication (TOTP) app to scan the QR code below.
        Then, enter the token generated by the app.
       
        Two-factor authentication is not enabled for your
        account. Enable two-factor authentication for enhanced account
        security.
       
        You are about to take your account security to the
        next level. Follow the steps in this wizard to enable two-factor
        authentication.
       
        You can use this form to assign user accounts to persons. Use the
        dropdowns to select existing accounts; use the text fields to create new
        accounts on-the-fly. The latter will create a new account with the
        entered username and copy all other details from the person.
       
        You didn't customise your dashboard so that you see the system default. Please click on "Edit dashboard" to
        customise your personal dashboard.
       
        You have only one backup token remaining.
       
        You have %(counter)s backup tokens remaining.
       
      Do you really want to delete the %(object_name)s "%(object)s"?
     
      There was an error accessing this page. You probably don't have an internet connection. Check to see if your WiFi
      or mobile data is turned on and try again. If you think you are connected, please contact the system
      administrators:
     
    the person %(person)s recently changed the following fields:
   
   the person %(person)s recently changed the following fields:
  
   the system detected some new problems with your data.
   Please take some time to inspect them and solve the issues or mark them as ignored.
   
  the system detected some new problems with your data.
  Please take some time to inspect them and solve the issues or mark them as ignored.
  %(person)s changed their data! 2FA <= 600 px, 12 columns <strong>Note:</strong> you can still <a href="%(email_url)s">change your e-mail address</a> > 1200 px>, 12 columns > 600 px, 12 columns > 992 px, 12 columns About AlekSIS About AlekSIS — The Free School Information System Account Account Security Account data Account inactive Account inactive. Actions Activate Widget Activities Activity Add Backup Phone Add Phone Number Add a Third-party Account Additional data Additional fields Additional name(s) Address Addtitional field for groups Addtitional fields for groups Admin Advanced personal data Affected object Age range AlekSIS – The Free School Information System Allow Allow users to change their passwords Allow users to edit their dashboard Already have an account? Then please <a href="%(login_url)s">sign in</a>. An unexpected error has
          occured. Announcement Announcement recipient Announcement recipients Announcements Application Are you sure to delete the application %(application_name)s? Are you sure to revoke the access for this application? As a last resort, you can use a backup token: Assign child groups to groups Authentication Authorization Grant Type Authorize Authorized applications Automatically create new persons for new users Automatically link existing persons to new users by their e-mail address Available languages Available widgets Average age Back Back to Account Security Back to Profile Back to login Backend Admin Backup Phone Numbers Backup Tokens Bad token Base data Boolean (Yes/No) Boolean or empty (Yes/No/Neither) Can add oauth applications Can assign child groups to groups Can change group preferences Can change person preferences Can change site preferences Can delete oauth applications Can edit default dashboard Can impersonate Can link persons to accounts Can list oauth applications Can manage data Can run data checks Can solve data check problems Can test PDF generation Can update oauth applications Can use search Can view address Can view contact details Can view oauth applications Can view personal details Can view persons groups Can view photo Can view statistics about group. Can view system status Cancel Celery task results Change password Change preferences Changed by Changing of password disabled Changing of password disabled. Channels to use for notifications Check data again Children Clear Clear all filters Client id Client secret Client type Common data Configuration Confirm Connections Consents Contact data Contact details Contact for notification if a person changes their data Count of members Count of objects with new problems Create %(name)s Create %(widget)s Create OAuth2 Application Create a new account Create additional field Create dashboard widget Create group Create group type Create person Create school term Current group: Currently selected groups Custom menu Custom menu item Custom menu items Custom menus Dashboard Dashboard Widget Dashboard Widgets Dashboard widget Dashboard widget order Dashboard widget orders Dashboard widgets Data check result Data check results Data checks Data management Date Date and time Date and time from when to show Date and time until when to show Date done Date of birth Deactivate DashboardWidget Dear %(notification_user)s, Debug mode disabled Debug mode enabled Decimal number Default dashboard Delete Delete %(object_name)s Delete application Description Detected problem Detected problems Disable Disable Two-Factor Authentication Disallow Display name of the school Download PDF E-Mail E-mail address Edit Edit %(widget)s Edit additional field Edit announcement Edit application Edit dashboard Edit default dashboard Edit group Edit group type Edit person Edit school term Editable fields on person model which should trigger a notification on change Email recipient groups for data checks problem emails Email recipients for data checks problem emails Enable Two-Factor Authentication Enable signup Enabled custom authentication backends End date English Ensure that there are no broken DashboardWidgets. Error Everyone can access the site. Everything is fine. Existing account External link widget External link widgets Favicon Field on person to match primary group against Fields on person model which are editable by themselves. File expires at Filter groups Filter persons First name Forgot Password? Forgot your current password? Click here to reset it: Forgotten your password? Enter your e-mail address below, and we'll send you an e-mail allowing you to reset it. Free/Open Source Licence From when until when should the announcement be displayed? Full licence text Generate Tokens Generate backup codes Generated HTML file Generated PDF file Generating PDF file … German Global Search Go back Group Group type Group types Groups Groups and child groups Guardians / Parents Hello! Hello, Home Home phone I have read the <a href='{privacy_policy}'>Privacy policy</a> and agree with them. ID IP address Icon Icon URL If you don't have any device with you, you can access
        your account using backup tokens. If your primary method is not available, we are able to
        send backup tokens to the phone numbers listed below. Ignore problem Impersonate Impersonate user Impersonation Impress Integer Internationalisation Is person active? Issue solved Language Last activities Last backup {time_gone_since_backup}! Last name Licence information Link Link persons to accounts Link to detailed view Link to imprint Link to privacy policy Linked school term Linked user Logged in as Login Login cancelled Login with username and password Logo Logout Long name Mail out address Mail out name Maintenance mode disabled Maintenance mode enabled Me Members Menu Menu ID Mobile phone More information More information about the EUPL More information → My preferences Name Name format for addressing Network error New account New notification for New user Next No activities available yet. No applications defined. No authorized applications. No backup found! No backup result found! No internet
    connection. No notifications available yet. No search results could be found to your search. No valid selection. Notification Notification sent Notifications OAuth2 Applications OAuth2 applications Official name of the school, e.g. as given by supervisory authority Options to solve the problem Or, alternatively, use one of your backup phones: Order Other Licence Owner Owners PDF file PDF file expiration duration PDF files PWA-Icon Parent groups Part of the default dashboard Password Password (again) Password changed! Password reset mail sent Password reset mail sent! People Permission Denied Person Persons Persons and accounts Photo Place Please be careful! Please confirm that <a href="mailto:%(email)s">%(email)s</a> is an e-mail address for user %(user_display)s. Please contact one of your site administrators, if you
        have any trouble resetting your password: Please enter a search term above. Please go through all data and check whether some extra action is
          needed. Please login to see this page. Please return to your application and enter this code: Postal code Powered by AlekSIS Preferences Preferences for %(instance)s Primary colour Primary group Primary method: %(primary)s Privacy Policy Problem description Progress: Generate PDF file Progress: Run data checks Publish announcement Publish new announcement Read Recent notifications Recipient Recipients Redirect URIs Register new applications Registered checks Regular expression to match primary group, e.g. '^Class .*' Related data check task Remove Reset password Results Revoke Revoke access Run data checks … SMS Save Save and next Save preferences Save und publish announcement School term School terms Search Search Term Search by contact details Search by name Secondary colour Select language Selected groups Selected persons Send emails if data checks detect problems Sender Sent Service Set password Sex Short name Show Codes Show dashboard to users without login Show details Show object Sign up Signup Signup closed Signup closed. Site description Site preferences Site title Size on desktop devices Size on large desktop devices Size on mobile devices Size on tablet devices Solve option '{solve_option_obj.verbose_name}'  Source code Start assigning child groups for this groups Start date Statistics Status Stop impersonation Street Street number Success! System checks System health checks System status Task Task result Task user Task user assignment Task user assignments Test PDF generation Text (multi-line) Text (one line) The DashboardWidget was reported broken automatically. The PDF file has been generated successfully. The additional field has been deleted. The additional_field has been saved. The announcement has been deleted. The announcement has been saved. The application requests access to the following scopes: The backup folder doesn't exist. The child groups were successfully saved. The configuration of the default dashboard has been saved successfully. The dashboard widget has been created. The dashboard widget has been deleted. The dashboard widget has been saved. The data checks were run successfully. The from date and time must be earlier then the until date and time. The group has been deleted. The group has been saved. The group type has been deleted. The group type has been saved. The maintenance mode is currently enabled. Please try again
          later. The page you requested, enforces users to verify using
          two-factor authentication for security reasons. You need to enable these
          security features in order to access this page. The person has been deleted. The person has been saved. The preferences have been saved successfully. The requested page or object was not
          found. The school term has been created. The school term has been saved. The solve option '{solve_option_obj.verbose_name}'  The start date must be earlier than the end date. The system detected some new problems with your data. The system detected some problems with your data. The system hasn't detected any problems with your data. The third-party account could not be disconnected because it is the only login method available. The third-party account has been successfully disconnected. There are no announcements. There are unresolved data problems. There is already a school term for this time or a part of this time. There was a problem while generating the PDF file. There was a problem while running data checks. Third-party Account Login Failure Third-party Account Login Failure. Third-party accounts This e-mail confirmation link expired or is invalid. Please <a href="%(email_url)s">issue a new e-mail confirmation request</a>. This username is already in use. This widget is currently not available. Time Time taken Title Title of field Title of type Tokens will be generated by your YubiKey. Tokens will be generated by your token generator. Two-Factor Authentication successfully enabled Two-factor authentication is not enabled for your
          account. Enable two-factor authentication for enhanced account
          security. Type of field Type of group URL URL / Link Unknown Unregister Update Update selection Use Backup Token Use alternative login options User Valid from Valid until Verify your email address Verify your email! We've sent a token to your phone number. Please
      enter the token you've received. Website of AlekSIS Who should see the announcement? Widget Title Widget is broken Write your announcement: You are about to disable two-factor authentication. This weakens your account security, are you sure? You are about to use your %(provider_name)s account to login to
        %(site_name)s. As a final step, please complete the following form: You are not allowed to access the requested page or
          object. You are not allowed to create announcements which are only valid in the past. You cannot set a new username when also selecting an existing user. You currently have no third-party accounts connected to this account. You don't have any backup codes yet. You have no permission to view this page. Please login with an other account. You must type the same password each time. You need at least one recipient. You'll be adding a backup phone number to your
      account. This number will be used if your primary method of
      registration is not available. Your AlekSIS team Your dashboard Your dashboard configuration has been saved successfully. female in minutes male seconds we got a new notification for you: years  years to {task.status} - {task.result} Project-Id-Version: AlekSIS (School Information System) 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-06-12 10:28+0000
Last-Translator: Jonathan Weth <teckids@jonathanweth.de>
Language-Team: German <https://translate.edugit.org/projects/aleksis/aleksis/de/>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.4
 
                      Bitte geben Sie den von Ihrem Token-Generator
              generierten Token ein.
                     
                      Wir rufen Ihr Telefon jetzt an, 
            bitte geben Sie die Zahlen ein, die Sie hören.
                     
                      Wir haben Ihnen per SMS einen Token geschickt, 
            bitte geben Sie diesen ein.
                     
                    Diese App ist unter %(licence)s lizenziert.
                   
                    Nutzen Sie dieses Formular um Ihre Backup-Tokens zum Anmelden einzugeben.
                Diese Tokens wurden für Sie generiert, um diese gut aufzubewahren. Bitte
                geben Sie einen dieser Tokens ein, um sich einzuloggen.
                   
                Debug-Modus ist deaktiviert. Standard-Fehlerseiten werden bei Fehlern angezeigt.
               
                Anmelden mit %(name)s
               
                Nur Administratoren und Besucher von internen IP-Adressen können die Seite aufrufen.
               
                Der Server gibt Debug-Informationen bei Fehlern zurück. Nicht im Produktivbetrieb nutzen!
               
              Wenn dieser Fehler bestehen bleibt,
kontaktieren Sie bitte einen der Administratoren:
             
              Der Core und die offiziellen Apps von AlekSIS sind unter der EUPL, Version 1.2 oder später, lizenziert. Für Lizenzinformationen
zu Apps von Drittanbietern, wenn installiert, siehe direkt bei der jeweiligen App weiter unten auf dieser Seite. Die Lizenzen
sind wie folgt markiert:
             
              Der Link zum Zurücksetzen des Passwortes war falsch, wahrscheinlich, weil er bereits benutzt wurde. Bitte starten Sie eine neue Anfrage <a href="%(passwd_reset_url)s"              class="blue-text text-lighten-2">zum Zurücksetzen des Passwortes</a>.
             
              Diese Plattform wird mit AlekSIS, einem webbasierten Schulinformationssystem (SIS), 
welches für die Verwaltung und/oder Veröffentlichung von Bildungseinrichtungen verwendet werden kann.
AlekSIS ist freie Software und kann von jedem benutzt werden.
             
              Gültig für %(from)s
             
              Gültig von %(from)s – %(until)s
             
              Gültig von %(from)s bis %(until)s
             
              Ohne aktiviertes JavaScript kann der Fortschritt leider nicht aktualisiert werden.
             
            Beim dem Versuch, die Anmeldung über Ihr Drittanbieter-Konto durchzuführen, ist ein Fehler aufgetreten.
            Kontaktieren Sie bitte einen Ihrer Systemadministratoren:
           
            Wenn Sie auf "Zurück" oder "Weiter" klicken, werden die aktuellen Gruppenzuordnungen nicht gespeichert.
Wenn Sie auf "Speichern" klicken, werden alle existierenden Zuordnungen von Kindgruppen für diese Gruppe
mit dem überschrieben, was Sie auf dieser Seite ausgewählt haben.
           
            Wenn Sie der Meinung sind, dass es sich um einen Fehler in AlekSIS handelt, kontaktieren Sie bitte einen Ihrer
     Systemadministratoren:
           
            Wenn Sie über einen Link auf einer externen Seite hierher gelangt sind,
      ist es möglich, dass dieser veraltet war.
           
            Anmelden mit %(name)s
           
            Bitte wählen Sie Gruppen aus, um Gruppen zuzuordnen.
           
            Von %(trans_sender)s um %(trans_created_at)s
         
            Das System wird nach folgenden Problemen suchen:
           
            Es ist ein Problem dabei aufgetreten, das Widget "%(title)s" zu laden.
Sie brauchen nichts weiter machen.
           
            Dieses Konto ist aktuell inaktiv. Wenn Sie denken,
dass dies ein Fehler ist, kontaktieren Sie einen der Administratoren:
           
            Diese Seite ist aktuell nicht erreichbar. Wenn dieser Fehler bestehen bleibt, kontaktieren Sie bitte einen Ihrer Systemadministratoren:
           
            Dieser Teil der Anwendung setzt voraus, dass wir verifizieren, dass Sie die Person sind, die sie vorgeben, zu sein.
Zu diesem Zweck setzen wir voraus, dass Sie die Inhaberschaft Ihrer E-Mail-Adresse bestätigen.
           
            Die Registrierung ist aktuell geschlossen. Wenn Sie denken, dass dies ein Fehler ist,
 kontaktieren Sie bitte einen Ihrer Systemadministratoren.
           
            Benutzer dürfen ihre eigenen Passwörter nicht ändern. Wenn Sie denken, 
dass dies ein Fehler ist, kontaktieren Sie bitte einen der Administratoren:
           
            Wir rufen Ihr Telefon jetzt an, bitte geben Sie die Zahlen ein, die Sie hören.
           
            Wir haben Ihnen eine E-Mail zur Verifizierung geschickt.
Bitte klicken Sie auf den Link in dieser E-Mail.
Bitte kontaktieren Sie uns, wenn Sie diese nicht binnen weniger Minuten erhalten.
           
            Wir haben Ihnen eine E-Mail gesendet. Bitte kontaktieren Sie einen der Administratoren,
wenn Sie diese nicht innerhalb weniger Minuten erhalten.
           
            Wir haben Ihnen per SMS einen Token geschickt, bitte geben Sie diesen ein.
           
            Sie haben sich entschieden, die Anmeldung mit einem Ihrer bestehenden Konten bei uns abzubrechen. Wenn dies ein Fehler war, <a href="%(login_url)s">fahren Sie bitte mit dem Login fort</a>.
           
            Ihr Administratorenkonto ist mit keiner Person verknüpft. Deshalb
            wurde Ihr Konto mit einer Dummyperson verknüpft.
           
            Ihr Password wurde geändert!
           
            Ihre Administratoren werden automatisch über diesen Fehler informiert.
      Sie können diese auch direkt kontaktieren:
           
            Ihr Benutzerkonto ist nicht mit einer Person verknüpft. Das bedeutet, dass Sie
        keine schulbezogenen Informationen aufrufen können. Bitte wenden Sie sich an
        die Verwaltenden von AlekSIS an Ihrer Schule.
           
          Es kann passieren, dass Sie keinen Zugriff auf Ihren Tokengenerator haben. 
Um die Wiederherstellung zu aktivieren,
generieren Sie Backupcodes oder fügen eine Telefonnummer hinzu.
         
          Keine Drittanbieter verfügbar.
         
          Auf dieser Seite können Sie Ihr das Standard-Dashboard zusammenstallen, welches angezeigt wird, wenn ein Benutzer kein eigenes definiert. 
Sie können beliebige Elemente von den "Verfügbaren Widgets" in "Standard-Dashboard" ziehen oder die Reihenfolge verändern, indem Sie die Widgets bewegen. 
Wenn Sie fertig sind, vergessen Sie bitte nicht, auf "Speichern" zu drücken.
         
          Auf dieser Seite können Sie Ihr persönliches Dashboard zusammenstallen. Sie können beliebige Elemente von den "Verfügbaren Widgets" 
in "Ihr Dashboard" ziehen oder die Reihenfolge verändern, indem Sie die Widgets bewegen. Wenn Sie fertig sind, vergessen Sie bitte nicht, 
auf "Speichern" zu drücken.
         
          Drucken Sie diese Tokens aus und bewahren Sie sie gut auf.
         
          Mit der ausgewählten Authentifizierungsmethode ist ein Fehler aufgetreten. 
Bitte gehen Sie zurück und überprüfen, dass Sie die Informationen korrekt eingegeben haben, versuchen Sie es erneut,
oder benutzen Sie stattdessen eine andere Authentifizierungsmethode. 
Wenn der Fehler bestehen bleibt, kontaktieren Sie bitte einen der Administratoren.
         
          Sie können diese Seite verwenden, um Kindgruppen zu Gruppen zuzuordnen. Bitte nutzen Sie die folgenden Filter, um die Gruppen auszuwählen, die Sie 
          ändern möchten und klicken auf "Weiter".
         
        Backup-Token können genutzt werden, wenn Ihre primären und Backup-
        Telefonnummern nicht verfügbar sind. Die Backup-Tokens unten können für
        die Anmeldungsverifizierung genutzt werden. Wenn Sie alle Backup-Tokens genutzt haben,
        müssen Sie neue generieren. Nur gültige Backup-Tokens werden angezeigt.
       
        Gratulation, Sie haben die Zwei-Faktor-Authentifizierung erfolgreich aktiviert.
       
        Wir raten Ihnen jedoch dringend davon ab.
        Sie können jedoch auch die Zwei-Faktor-Authentifizierung für Ihr Konto deaktivieren.
       
        Bitte geben Sie die Telefonnummer an, die wir anrufen sollen.
Diese Nummer wird im nächsten Schritt überprüft.
       
        Bitte geben Sie die Telefonnummer des Gerätes an,
        an die die SMS-Nachrichten geschickt werden sollen. Diese Nummer wird im nächsten Schritt überprüft.
       
        Bitte wählen Sie aus, welche Authentifikationsmethode Sie nutzen wollen:
       
        Von %(trans_sender)s am %(trans_created_at)s
     
        Diese einfache Seite kann genutzt werden, um die korrekte Funktionalität des eingebauten PDF-Generierungssystem zu testen.
       
        Um Ihren YubiKey zu identifizieren und verifizieren, 
geben Sie bitte ein Token ein. 
Dann wird Ihr YubiKey mit Ihrem Konto verknüpft.
       
        Um mit dem Codegenerator zu starten, benutzen Sie bitte Ihre
App für Zwei-Faktor-Authentifizierung (TOTP), um diesen QR-Code zu scannen.
Dann geben Sie den in der App angezeigten Code an:
       
        Die Zwei-Faktor-Authentifizierung ist nicht für Ihren Account aktiviert.
          Aktivieren Sie Zwei-Faktor-Authentifizierung für eine verbesserte
          Accountsicherheit.
       
        Sie sind dabei, Ihre Accountsicherheit auf das
       nächste Level zu erhöhen. Bitte folgen Sie den Schritten im Wizard um die
       Zwei-Faktor-Authentifizierug zu aktivieren.
       
        Sie können dieses Formular nutzen, um Benutzerkonten Personen zuzuweisen. Nutzen Sie das
    Auswahlfeld um ein existierendes Benutzerkonto auszuwählen; nutzen Sie das Textfeld, um einen neuen Benutzer zu
    erstellen. Letzteres erstellt ein neues Benutzerkonto mit dem
    eingegebenen Benutzernamen und kopiert alle anderen Daten der Person.
       
        Sie haben Ihr Dashboard nicht angepasst, sodass Sie das Standard-Dashboard sehen.
Bitte klicken Sie auf "Dashboard bearbeiten", um Ihr persönliches Dashboard anzupassen.
       
        Sie haben keinen Backup-Token übrig.
       
        Sie haben %(counter)s Backup-Tokens übrig.
       
      Möchten Sie wirklich %(object_name)s "%(object)s" löschen?
     
      Es ist ein Fehler beim Aufrufen der Seite aufgetreten. Eventuell haben Sie keine Internetverbindung. Bitte prüfen Sie, ob WLAN oder mobile Daten aktiv sind, 
      und probieren Sie es erneut. Wenn Sie der Meinung sind, dass Sie mit dem Internet verbunden sind, kontaktieren Sie bitte einen Ihrer 
      Systemadministratoren:
     
    die Person %(person)s hat kürzlich die folgenden Felder geändert:
   
   die Person %(person)s hat kürzlich die folgenden Felder geändert:
  
   das System hat einige neue Probleme mit Ihren Daten entdeckt.
Bitte nehmen Sie sich etwas Zeit, diese zu überprüfen und sie zu lösen oder als ignoriert zu markieren.
   
  das System hat einige neue Probleme mit Ihren Daten entdeckt.
Bitte nehmen Sie sich etwas Zeit, diese zu überprüfen und sie zu lösen oder als ignoriert zu markieren.
  %(person)s hat Daten verändert! 2FA <= 600 px, 12 Spalten <strong>Hinweis:</strong> Sie können immer noch <a href="%(email_url)s"> Ihre E-Mail-Adresse ändern</a> > 1200 px, 12 Spalten > 600px, 12 Spalten > 992 px, 12 Spalten Über AlekSIS Über AlekSIS — The Free School Information System Konto Kontosicherheit Kontodaten Konto inaktiv Konto inaktiv. Aktionen Widget aktivieren Aktivitäten Aktivität Backup-Telefon hinzufügen Telefonnummer hinzufügen Ein Drittanbieter-Konto hinzufügen Zusätzliche Datne Zusätzliche Felder Zusätzliche Namen Adresse Zusätzliche Felder für Gruppen Zusätzliche Felder für Gruppen Admin Zusätzliche persönliche Daten Betroffenes Objekt Altersbereich AlekSIS – The Free School Information System Erlauben Erlaube Benutzern, ihr Passwort zu ändern Erlaube Benutzern, ihr Dashboard zu bearbeiten Haben Sie bereits ein Konto? Dann <a href="%(login_url)s">melden Sie sich bitte an</a>. Ein unerwarteter Fehler ist
            aufgetreten. Ankündigung Empfänger der Ankündigung Empfänger der Ankündigung Ankündigungen Anwendung Sind Sie sicher, dass Sie die Anwendung %(application_name)s löschen möchten? Sind Sie sicher, dass Sie den Zugriff für diese Anwendung zurückziehen möchten? Als letzte Möglichkeit können Sie einen Backup-Token nutzen: Kindgruppen zu Gruppen zuordnen Authentifizierung Authorization Grant-Typ Autorisieren Autorisierte Anwendungen Erstelle automatisch neue Personen für neue Benutzer Verknüpfe existierende Personen automatisch mit neuen Personen anhand ihrer E-Mail-Adresse Verfügbare Sprachen Verfügbare Widgets Durchschnittsalter Zurück Zurück zur Kontosicherheit Zurück zum Profil Zurück zur Anmeldung Backend-Administration Backup-Telefonnummern Backup-Token Ungültiges Token Basisdaten Boolean (Ja/Nein) Boolean oder leer (Ja/Nein/weder) Kann OAuth-Anwendungen hinzufügen Kann Kindgruppen zu Gruppen zuordnen Kann Einstellungen einer Gruppe verändern Kann Einstellungen einer Person verändern Kann Konfiguration ändern Kann OAuth-Anwendungen löschen Kann Standarddashboard bearbeiten Kann sich verkleiden Kann Personen mit Benutzerkonten verknüpfen Can OAuth-Anwendungen auflisten Kann Daten verwalten Kann Datenprüfungen ausführen Kann Datenprüfungsprobleme lösen Kann die PDF-Generierung testen Kann OAuth-Anwendungen aktualisieren Kann Suche benutzen Kann Adresse sehen Kann Kontaktdetails sehen Kann OAuth-Anwendungen sehen Kann persönliche Daten sehen Kann Gruppen einer Person sehen Kann Foto sehen Kann Statistiken über Gruppen sehen. Kann Systemstatus sehen Abbrechen Celery Task-Ergebnisse Passwort ändern Einstellungen ändern Verändert von Passwortänderung deaktiviert Passwortänderung deaktiviert. Aktivierte Benachrichtungskanäle Daten erneut prüfen Kinder Zurücksetzen Alle Filter leeren Client-ID Client-Secret Client-Typ Allgemeine Daten Konfiguration Bestätigen Verbindungen Zustimmungen Kontaktdaten Kontaktdetails Kontakt für Benachrichtigung, wenn eine Person ihre Daten ändert Anzahl der Mitglieder Anzahl der Objekte mit neuen Problemen %(name)s erstellen %(widget)s erstellen OAuth2-Anwendung erstellen Neues Benutzerkonto erstellen Zusätzliches Feld erstellen Dashboard-Widget erstellen Gruppe erstellen Gruppentyp erstellen Person erstellen Schuljahr erstellen Aktuelle Gruppe: Aktuell ausgewählte Gruppen Benutzerdefiniertes Menü Benutzerdefiniertes Menüelement Benutzerdefinierte Menüelemente Benutzerdefinierte Menüs Dashboard Dashboard-Widget Dashboard-Widgets Dashboard-Widget Reihenfolge der Dashboard-Widgets Reihenfolgen der Dashboard-Widgets Dashboard-Widgets Datenprüfungsergebnis Datenprüfungsergebnisse Datenprüfungen Datenverwaltung Datum Datum und Uhrzeit Datum und Uhrzeit des Anzeigestarts Anzeigezeitraum Erledigungszeitpunkt Geburtsdatum Dashboard-Widget deaktivieren Liebe(r) %(notification_user)s, Debug-Modus deaktivert Debug-Modus aktiviert Dezimalzahl Standard-Dashboard Löschen %(object_name)s löschen Anwendung löschen Beschreibung Entdecktes Problem Gefundene Probleme Deaktivieren Zwei-Faktor-Authentifizierung deaktiveren Verbieten Sichtbarer Name der Schule PDF herunterladen E-Mail E-Mail-Adresse Bearbeiten %(widget)s bearbeiten Zusätzliches Feld bearbeiten Ankündigung bearbeiten Anwendung bearbeiten Dashboard bearbeiten Standard-Dashboard bearbeiten Gruppe editieren Gruppentyp editieren Person editieren Schuljahr bearbeiten Editierbare Felder des Personen-Models welche eine Benachrichtigung für Änderungen auslösen soll E-Mail-Empfängergruppen für Datenprüfungsproblem-E-Mails E-Mailempfänger für Datenprüfungsproblem-E-Mails Zwei-Faktor-Authentifizierung aktivieren Registrierung aktivieren Benutzerdefinierte Authentifizierungsbackends aktivieren Enddatum Englisch Sicherstellen, dass es keine kaputten Dashboard-Widgets gibt. Fehler Jeder kann die Seite aufrufen. Alles ist gut. Existierendes Konto Externer-Link-Widget Externer-Link-Widgets Favicon Feld um Primärgruppen zu finden Felder des Personen-Models welche von ihnen selbst editierbar sind. Datei abgelaufen am Gruppen filtern Personen filtern Vorname Passwort vergessen? Haben Sie Ihr aktuelles Passwort vergessen? Klicken Sie hier, um es zurückzusetzen: Passwort vergessen? Geben Sie Ihre E-Mail-Adresse hier ein und wir werden Ihnen eine E-Mail zum Zurücksetzen des Passwortes schicken. Freie/Open Source Lizenz Von wann bis wann soll die Ankündigung angezeigt werden? Kompletter Lizenztext Tokens generieren Backup-Codes generieren Generierte HTML-Datei Generierte PDF-Datei PDF-Datei wird generiert … Deutsch Globale Suche Zurück Gruppe Gruppentyp Gruppentypen Gruppen Gruppen und Kindgruppen Erziehungsberechtigte / Eltern Hallo! Hallo, Startseite Festnetz Ich habe die <a href='{privacy_policy}'>Datenschutzerklärung</a> gelesen und stimme ihr zu. ID IP-Adresse Symbol Symbol-URL Wenn Sie keines Ihrer Geräte dabei haben, können Sie sich
      mit Backup-Tokens einloggen. Wenn Ihre primäre Methode nicht verfügbar ist, sind wir in der Lage,
        Backup-Tokens zu den unten aufgelisteten Telefonnummern zu schicken. Problem ignorieren Verkleiden Als Benutzer verkleiden Verkleidung Impressum Ganze Zahl Internationalisierung Ist die Person aktiv? Problem gelöst Sprache Letzte Aktivitäten Letztes Backup: {time_gone_since_backup}! Nachname Lizenzinformationen Link Personen mit Benutzerkonten verknüpfen Link zur detaillierten Ansicht Link zum Impressum Link zur Datenschutzerklärung Zugeordnetes Schuljahr Verknüpfter Benutzer Angemeldet als Anmelden Login abgebrochen Anmeldung mit Benutzername und Passwort Logo Abmelden Langname E-Mail-Ausgangsadresse Ausgangsmailname Wartungsmodus deaktiviert Wartungsmodus aktiviert Ich Mitglieder Menü Menü-ID Handy Mehr Informationen Weitere Informationen über die EUPL Mehr Informationen → Meine Einstellungen Name Namensformat für Anreden Netzwerkfehler Neues Konto Neue Benachrichtigung für Neuer Benutzer Weiter Aktuell keine Aktivitäten verfügbar. Keine Anwendungen definiert. Keine autorisierten Anwendungen. Kein Backup gefunden! Kein Backupergebnis gefunden! Keine
    Internetverbindung. Aktuell keine Benachrichtigungen verfügbar. Es konnten keine Suchergebnisse zu Ihrem Suchausdruck gefunden werden. Keine gültige Auswahl. Benachrichtigung Benachrichtigung gesendet Benachrichtigungen OAuth2-Anwendungen OAuth2-Anwendungen Offizieller Name der Schule, wie er z.B. von der Behörde vorgegeben ist Optionen, das Problem zu lösen Oder, alternativ, nutzen Sie eins Ihrer Backup-Telefone: Reihenfolge Andere Lizenz Leiter Leiter/-innen PDF-Datei PDF-Datei-Ablaufdauer PDF-Dateien PWA-Icon Übergeordnete Gruppen Teil des Standarddashboards Passwort Passwort wiederholen Passwort geändert! E-Mail zum Zurücksetzen des Passwortes versendet E-Mail zum Zurücksetzen des Passwortes versendet! Leute Zugriff verwehrt Person Personen Personen und Konten Foto Ort Bitte seien Sie vorsichtig! Bitte bestätigen Sie, dass <a href="mailto:%(email)s">%(email)s</a> eine E-Mail-Adresse für den Benutzer %(user_display)s ist. Bitte kontaktieren Sie einen der Administratoren, 
wenn Sie irgendwelche Probleme beim Zurücksetzen des Passwortes haben: Bitte geben Sie einen Suchausdruck ein. Bitte gehen Sie alle Daten durch und prüfen Sie, ob weitere Aktionen
notwendig sind. Bitte melden Sie sich an, um diese Seite zu sehen. Bitte gehen Sie zurück in Ihre Anwendung und geben Sie diesen Code ein: Postleitzahl Betrieben mit AlekSIS Einstellungen Einstellungen für %(instance)s Primärfarbe Primärgruppe Primäre Methode: %(primary)s Datenschutzerklärung Problembeschreibung Fortschritt: PDF-Datei generieren Fortschritt: Datenprüfungen ausführen Ankündigung veröffentlichen Neue Ankündigung veröffentlichen Gelesen Letzte Benachrichtigungen Empfänger Empfänger Weiterleitungs-URLs Neue Anwendungen registrieren Registrierte Prüfungen Regulärer Ausdruck um Primärgruppen zu finden, z. B.  '^Class .*' Zugehörige Datenprüfungsaufgabe Löschen Passwort zurücksetzen Ergebnisse Zurückziehen Zugriff zurückziehen Datenprüfungen laufen … SMS Speichern Speichern und weiter Einstellungen speichern Ankündigung speichern und veröffentlichen Schuljahr Schuljahre Suchen Suchausdruck Nach Kontaktdetails suchen Nach Namen suchen Akzentfarbe Sprache auswählen Ausgewählte Gruppen Ausgewählte Personen E-Mails versenden, wenn Datenprüfungen Probleme finden Absender Versandt Dienst Password setzen Geschlecht Kurzname Codes anzeigen Zeige Dashboard für Benutzer ohne Login Details anzeigen Objekt anzeigen Registrieren Registrierung Registrierung geschlossen Registrierung geschlossen. Seitenbeschreibung Seiteneinstellungen ändern Seitentitel Größe auf Desktopgeräten Größe auf großen Desktopgeräten Größe auf Mobilgeräten Größe auf Tablets Lösungsoption "{solve_option_obj.verbose_name}"  Quellcode Zuordnung von Kindgruppen zu Gruppen starten Startdatum Statistiken Status Verkleidung beenden Straße Hausnummer Erfolg! Systemprüfungen Systemprüfungen Systemstatus Task Task-Ergebnis Task-Benutzer Task-Benutzer-Zuordnung Task-Benutzer-Zuordnungen PDF-Generierung testen Text (mehrzeilig) Text (eine Zeile) Das Dashboard-Widget wurde automatisch als kaputt gemeldet. Die PDF-Datei wurde erfolgreich generiert. Das zusätzliche Feld wurde gelöscht. Das zusätzliche Feld wurde gespeichert. Ankündigung wurde gelöscht. Die Ankündigung wurde gespeichert. Die Anwendung fordert Zugriff auf die folgenden Bereiche an: Der Backup-Ordner existiert nicht. Die Untergruppen wurden gespeichert. Die Konfiguration des Standard-Dashboardes wurde erfolgreich gespeichert. Das Dashboard-Widget wurde erstellt. Das Dashboard-Widget wurde gelöscht. Das Dashboard-Widget wurde gespeichert. Die Datenprüfungen wurden erfolgreich ausgeführt. Das Startdatum und die Startzeit müssen vor dem Enddatum und der Endzeit sein. Die Gruppe wurde gelöscht. Die Gruppe wurde gespeichert. Der Gruppentyp wurde gelöscht. Der Gruppentyp wurde gespeichert. Der Wartungsmodus ist aktuell aktiviert. Bitte versuchen Sie es
            später erneut. Die von Ihnen gewünschte Seite erfordert aus Sicherheitsgründen
          eine Verifizierung durch Zwei-Faktor-Authentifizierung. Sie müssen diese
          Sicherheitsfunktion aktivieren, um diese Seite aufzurufen. Die Person wurde gelöscht. Die Person wurde gespeichert. Die Einstellungen wurde gespeichert. Die angefragte Seite oder das angefragte Objekt wurde nicht
            gefunden. Das Schuljahr wurde erstellt. Das Schuljahr wurde gespeichert. Die Lösungsoption "{solve_option_obj.verbose_name}"  Das Startdatum muss vor dem Enddatum liegen. Das System hat einige neue Probleme mit Ihren Daten entdeckt. Das System hat einige Problemen mit Ihren Daten gefunden. Das System hat keine Probleme mit Ihren Daten entdeckt. Das Drittanbieter-Konto konnte nicht deaktiviert werden, weil es die einzige verfügbare Anmeldeoption ist. Das Drittanbieter-Konto wurde erfolgreich getrennt. Es gibt aktuell keine Ankündigungen. Es gibt ungelöste Datenprobleme. Es gibt bereits ein Schuljahr für diesen Zeitraum oder einen Teilzeitraum. Es ist ein Fehler beim Generieren der PDF-Datei aufgetreten. Es gab ein Problem beim Ausführen der Datenprüfungen. Anmeldung über Drittanbieter-Konto fehlgeschlagen Anmeldung über Drittanbieter-Konto fehlgeschlagen. Drittanbieter-Konten Dieser E-Mail-Bestätigungslink ist abgelaufen oder nicht gültig. Bitte <a href="%(email_url)s">fragen Sie eine neue E-Mail-Bestätigung an</a>. Dieser Benutzername wird bereits genutzt. Das Widget ist aktuell nicht verfügbar. Zeit Dauer Titel Feldtitel Titel des Typs Tokens werden von Ihrem YubiKey generiert. Tokens werden von Ihrem Token-Generator generiert. Zwei-Faktor-Authentifizierung erfolgreich aktiviert Die Zwei-Faktor-Authentifizierung ist nicht für Ihren Account aktiviert.
          Aktivieren Sie Zwei-Faktor-Authentifizierung für eine verbesserte
          Accountsicherheit. Feldtyp Gruppentyp URL URL / Link Unbekannt Abmelden Aktualisieren Auswahl aktualisieren Backup-Token nutzen Alternative Anmeldemöglichkeiten nutzen Benutzer Gültig von Gültig bis Verifizieren Sie Ihre E-Mail-Adresse Verifizieren Sie Ihre E-Mail! Wir haben Ihnen einen Token an Ihre Telefonnummer gesendet.
      Bitte geben Sie diesen Token ein. Website von AlekSIS Wer soll die Ankündigung sehen? Widget-Titel Widget ist kaputt Schreiben Sie ihre Ankündigung: Sie sind dabei, Zwei-Faktor-Authentifizierung zu deaktivieren. Das verschlechtert Ihre Kontosicherheit. Sind Sie sicher? Sie sind dabei, Ihr %(provider_name)s-Konto zur Anmeldung bei %(site_name)s zu nutzen. 
Als ein letzter Schritt vervollständigen Sie bitte das folgende Formular: Es ist Ihnen nicht erlaubt, auf die angefragte Seite oder das angefragte
             Objekt zuzugreifen. Sie dürfen keine Ankündigungen erstellen, die nur für die Vergangenheit gültig sind. Sie können keine neuen Benutzer erstellen, wenn Sie gleichzeitig einen existierenden Benutzer auswählen. Sie haben aktuell keine Drittanbieter-Konten mit Ihrem Konto verbunden. Sie haben aktuell keine Backup-Codes. Sie haben nicht die nötigen Berechtigungen, um diese Seite aufzurufen. Bitte loggen Sie sich mit einem anderen Account ein. Sie müssen zweimal das gleiche Passwort eingeben. Sie benötigen mindestens einen Empfänger. Sie werden eine Backup-Telefonnummer zu Ihrem
      Account hinzufügen. Diese Nummer wird genutzt, wenn Ihre primäre
      Authentifikationsmethode nicht verfügbar ist. Ihr AlekSIS-Team Ihr Dashboard Ihre Dashboardkonfiguration wurde erfolgreich gespeichert. weiblich in Minuten männlich Sekunden wir haben eine neue Benachrichtigung für Sie: Jahre  Jahre bis {task.status} - {task.result} 