# -*- coding: utf-8 -*-
from setuptools import setup

packages = \
['aleksis',
 'aleksis.core',
 'aleksis.core.migrations',
 'aleksis.core.templatetags',
 'aleksis.core.tests.browser',
 'aleksis.core.tests.models',
 'aleksis.core.tests.templatetags',
 'aleksis.core.tests.views',
 'aleksis.core.util']

package_data = \
{'': ['*'],
 'aleksis.core': ['locale/ar/LC_MESSAGES/*',
                  'locale/de_DE/LC_MESSAGES/*',
                  'locale/fr/LC_MESSAGES/*',
                  'locale/la/LC_MESSAGES/*',
                  'locale/nb_NO/LC_MESSAGES/*',
                  'locale/tr_TR/LC_MESSAGES/*',
                  'static/*',
                  'static/icons/*',
                  'static/img/*',
                  'static/js/*',
                  'templates/*',
                  'templates/account/*',
                  'templates/account/email/*',
                  'templates/components/*',
                  'templates/core/*',
                  'templates/core/additional_field/*',
                  'templates/core/announcement/*',
                  'templates/core/dashboard_widget/*',
                  'templates/core/data_check/*',
                  'templates/core/group/*',
                  'templates/core/group_type/*',
                  'templates/core/management/*',
                  'templates/core/pages/*',
                  'templates/core/partials/*',
                  'templates/core/person/*',
                  'templates/core/school_term/*',
                  'templates/django_tables2/*',
                  'templates/dynamic_preferences/*',
                  'templates/impersonate/*',
                  'templates/material/*',
                  'templates/material/fields/*',
                  'templates/oauth2_provider/*',
                  'templates/search/*',
                  'templates/search/indexes/core/*',
                  'templates/sms/*',
                  'templates/socialaccount/*',
                  'templates/socialaccount/snippets/*',
                  'templates/templated_email/*',
                  'templates/two_factor/*',
                  'templates/two_factor/core/*',
                  'templates/two_factor/profile/*']}

install_requires = \
['Celery[redis,django]>=5.0.0,<6.0.0',
 'Django>=3.2.5,<4.0.0',
 'Whoosh>=2.7.4,<3.0.0',
 'bs4>=0.0.1,<0.0.2',
 'calendarweek>=0.5.0,<0.6.0',
 'celery-haystack-ng>=0.20,<0.21',
 'celery-progress>=0.1.0,<0.2.0',
 'colour>=0.1.5,<0.2.0',
 'django-allauth>=0.45.0,<0.46.0',
 'django-any-js>=1.1,<2.0',
 'django-bleach>=0.7.0,<0.8.0',
 'django-cachalot>=2.3.2,<3.0.0',
 'django-cache-memoize>=0.1.6,<0.2.0',
 'django-celery-beat>=2.2.0,<3.0.0',
 'django-celery-email>=3.0.0,<4.0.0',
 'django-celery-results>=2.0.1,<3.0.0',
 'django-ckeditor>=6.0.0,<7.0.0',
 'django-cleanup>=5.1.0,<6.0.0',
 'django-colorfield>=0.4.0,<0.5.0',
 'django-dbbackup>=3.3.0,<4.0.0',
 'django-debug-toolbar>=3.2,<4.0',
 'django-dynamic-preferences>=1.9,<2.0',
 'django-extensions>=3.1.1,<4.0.0',
 'django-favicon-plus-reloaded>=1.1.2,<2.0.0',
 'django-filter>=2.2.0,<3.0.0',
 'django-guardian>=2.2.0,<3.0.0',
 'django-hattori>=0.2,<0.3',
 'django-haystack==3.0',
 'django-health-check>=3.12.1,<4.0.0',
 'django-impersonate>=1.4,<2.0',
 'django-ipware>=3.0,<4.0',
 'django-js-reverse>=0.9.1,<0.10.0',
 'django-jsonstore>=0.5.0,<0.6.0',
 'django-maintenance-mode>=0.16.0,<0.17.0',
 'django-material>=1.6.0,<2.0.0',
 'django-menu-generator-ng>=1.2.3,<2.0.0',
 'django-model-utils>=4.0.0,<5.0.0',
 'django-oauth-toolkit>=1.5.0,<2.0.0',
 'django-phonenumber-field[phonenumbers]<5.2',
 'django-polymorphic>=3.0.0,<4.0.0',
 'django-prometheus>=2.1.0,<3.0.0',
 'django-redis>=4.12.1,<5.0.0',
 'django-reversion>=4.0.0,<5.0.0',
 'django-sass-processor>=1.0,<2.0',
 'django-tables2>=2.1,<3.0',
 'django-templated-email>=3.0.0,<4.0.0',
 'django-titofisto>=0.1.0,<0.2.0',
 'django-two-factor-auth[phonenumbers,yubikey,call,sms]>=1.12.1,<2.0.0',
 'django-uwsgi-ng>=1.1.0,<2.0.0',
 'django-yarnpkg>=6.0,<7.0',
 'django_select2>=7.1,<8.0',
 'django_widget_tweaks>=1.4.5,<2.0.0',
 'djangorestframework>=3.12.4,<4.0.0',
 'dynaconf[yaml,ini,toml]>=3.1,<4.0',
 'haystack-redis>=0.0.1,<0.0.2',
 'html2text>=2020.0.0,<2021.0.0',
 'ipython>=7.20.0,<8.0.0',
 'libsass>=0.21.0,<0.22.0',
 'license-expression>=1.2,<2.0',
 'psutil>=5.7.0,<6.0.0',
 'psycopg2>=2.8,<3.0',
 'python-gnupg>=0.4.7,<0.5.0',
 'rules>=2.2,<3.0',
 'spdx-license-list>=0.5.0,<0.6.0']

extras_require = \
{'ldap': ['django-auth-ldap>=2.2,<3.0'],
 's3': ['django-storages>=1.11.1,<2.0.0', 'boto3>=1.17.33,<2.0.0']}

entry_points = \
{'console_scripts': ['aleksis-admin = aleksis.core.__main__:aleksis_cmd']}

setup_kwargs = {
    'name': 'aleksis-core',
    'version': '2.0rc5',
    'description': 'AlekSIS (School Information System)\u200a—\u200aCore',
    'long_description': 'AlekSIS (School Information System)\u200a—\u200aCore (Core functionality and app framework)\n=================================================================================\n\nThis is the core of the AlekSIS framework and the official distribution\n(see below). It bundles functionality for all apps, and utilities for\ndevelopers and administrators.\n\nIf you are looking for the AlekSIS standard distribution, i.e. the complete\nsoftware product ready for installation and usage, please visit the `AlekSIS`_\nwebsite or the distribution repository on `EduGit`_.\n\nFeatures\n--------\n\nThe AlekSIS core currently provides the following features:\n\n* For users:\n\n * Authentication via OAuth applications\n * Configurable dashboard\n * Custom menu entries (e.g. in footer)\n * Global preferences\n * Global search\n * Group types\n * Manage announcements\n * Manage groups\n * Manage persons\n * Notifications via SMS email or dashboard\n * PWA with offline caching\n * Rules and permissions for users, objects and pages\n * Two factor authentication via Yubikey, OTP or SMS\n * User preferences\n * User registration, password changes and password reset\n\n* For admins\n\n * Asynchronous tasks with celery\n * Authentication via LDAP\n * Automatic backup of database, static and media files\n * Generic PDF generation with chromium\n * OAuth2 and OpenID Connect provider support\n * Serve prometheus metrics\n * System health and data checks\n\n* For developers\n\n * `aleksis-admin` script to wrap django-admin with pre-configured settings\n * Caching with Redis\n * Django REST framework for apps to use at own discretion\n * Injection of fields, methods, permissions and properties via custom `ExtensibleModel`\n * K8s compatible, read-only Docker image\n * Object-level permissions and rules with `django-guardian` and `django-rules`\n * Query caching with `django-cachalot`\n * Search with `django-haystack` and `Whoosh` backend\n * uWSGI and Celery via `django-uwsgi` in development\n\nLicence\n-------\n\n::\n\n  Copyright © 2017, 2018, 2019, 2020, 2021 Jonathan Weth <dev@jonathanweth.de>\n  Copyright © 2017, 2018, 2019, 2020 Frank Poetzsch-Heffter <p-h@katharineum.de>\n  Copyright © 2018, 2019, 2020, 2021 Julian Leucker <leuckeju@katharineum.de>\n  Copyright © 2018, 2019, 2020, 2021 Hangzhi Yu <yuha@katharineum.de>\n  Copyright © 2019, 2020, 2021 Dominik George <dominik.george@teckids.org>\n  Copyright © 2019, 2020, 2021 Tom Teichler <tom.teichler@teckids.org>\n  Copyright © 2019 mirabilos <thorsten.glaser@teckids.org>\n  Copyright © 2021 Lloyd Meins <meinsll@katharineum.de>\n  Copyright © 2021 magicfelix <felix@felix-zauberer.de>\n\n  Licenced under the EUPL, version 1.2 or later, by Teckids e.V. (Bonn, Germany).\n\nPlease see the LICENCE.rst file accompanying this distribution for the\nfull licence text or on the `European Union Public Licence`_ website\nhttps://joinup.ec.europa.eu/collection/eupl/guidelines-users-and-developers\n(including all other official language versions).\n\n.. _AlekSIS: https://aleksis.org\n.. _European Union Public Licence: https://eupl.eu/\n.. _EduGit: https://edugit.org/AlekSIS/official/AlekSIS\n',
    'author': 'Dominik George',
    'author_email': 'dominik.george@teckids.org',
    'maintainer': 'Jonathan Weth',
    'maintainer_email': 'dev@jonathanweth.de',
    'url': 'https://aleksis.org/',
    'packages': packages,
    'package_data': package_data,
    'install_requires': install_requires,
    'extras_require': extras_require,
    'entry_points': entry_points,
    'python_requires': '>=3.9,<4.0',
}


setup(**setup_kwargs)
